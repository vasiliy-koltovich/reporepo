import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Visitor {


    enum Field {

        ACCOUNT_ID (User.AccountId, EnumSet.of(BoolExpr.OR), EnumSet.of(Expr.Eq)),

        USER_USER_ACTIVITY_HAS_ALERTS(User.Activity + "[/.]" + UserActivity.HasAlerts , EnumSet.noneOf(BoolExpr.class), EnumSet.of(Expr.Eq)),

        USER_USER_ACTIVITY_LAST_LOGIN_DATE(User.Activity + "[/.]" + UserActivity.LastLogin, EnumSet.of(BoolExpr.AND), EnumSet.of(Expr.Eq, Expr.Ge, Expr.Le)),

        USER_LOGIN_ID (User.LoginId, EnumSet.of(BoolExpr.OR), EnumSet.of(Expr.Eq));

        private String propertyName;

        private EnumSet<BoolExpr> supportedOperations;
        private EnumSet<Expr> supportedExpressions;

        private static Map<Field, Pattern> patternsMap = Arrays.stream(Field.values()).collect(Collectors.toMap(f -> f, f -> Pattern.compile("^[a-z][a-zA-Z0-9]*$")));


        private Field(String propertyName, EnumSet<BoolExpr> supportedOperations, EnumSet<Expr> supportedExpressions) {
            this.propertyName = propertyName;
            this.supportedOperations = supportedOperations;
            this.supportedExpressions = supportedExpressions;
        }

        boolean support(BoolExpr boolExpr) {
            return supportedOperations.contains(boolExpr);
        }

        boolean support(Expr expr) {
            return supportedExpressions.contains(expr);
        }


//        		public static Field findFields(String propertyName) {
//                    for (Field field : Field.values()) {
//                        if (propertyName != null) {
//                            Pattern pattern = Pattern.compile(field.getPropertyName());
//                            if (pattern.matcher(propertyName).matches()) {
//                                return field;
//                            }
//                        }
//                    }
//        			throw new UnsupportedOperationException("Filter is not valid or supported: " + propertyName);
//        		}


        public static Field findField(String propertyName) {
            if (propertyName != null) {

                for (Map.Entry<Field, Pattern> entry : patternsMap.entrySet()) {

                    if (entry.getValue().matcher(propertyName).matches()) {
                        return entry.getKey();
                    }

                }
            }
            throw new UnsupportedOperationException("Filter is not valid or supported: " + propertyName);

        }

    }

    enum BoolExpr {
        AND,
        OR
    }

    enum Expr {
        Eq,
        Gt,
        Ge,
        Lt,
        Le
    }

}
