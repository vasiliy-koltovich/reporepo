public class User {

    public static final String LoginId = "LoginId";
    public static final String UserKey = "UserKey";
    public static final String Domain = "Domain";
    public static final String Name = "Name";
    public static final String Credentials = "Credentials";
    public static final String Nickname = "Nickname";
    public static final String UserCategory = "UserCategory";
    public static final String Role = "Role";
    public static final String AccountId = "AccountId";
    public static final String Activity = "Activity";
    public static final String AdditionalInfo = "AdditionalInfo";
    public static final String UserGroup = "UserGroup";

    public static String getLoginId() {
        return LoginId;
    }

    public static String getUserKey() {
        return UserKey;
    }

    public static String getDomain() {
        return Domain;
    }

    public static String getName() {
        return Name;
    }

    public static String getCredentials() {
        return Credentials;
    }

    public static String getNickname() {
        return Nickname;
    }

    public static String getUserCategory() {
        return UserCategory;
    }

    public static String getRole() {
        return Role;
    }

    public static String getAccountId() {
        return AccountId;
    }

    public static String getActivity() {
        return Activity;
    }

    public static String getAdditionalInfo() {
        return AdditionalInfo;
    }

    public static String getUserGroup() {
        return UserGroup;
    }
}
