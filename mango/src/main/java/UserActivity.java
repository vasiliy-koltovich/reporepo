import java.util.Date;

public class UserActivity {

    private static final long serialVersionUID = 54L;
    public static final String LastLogin = "LastLogin";
    public static final String HasAlerts = "HasAlerts";

    private Date lastLogin;

    private Boolean hasAlerts;

    public UserActivity() {
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getLastLogin() {
        return this.lastLogin;
    }

    public void setHasAlerts(Boolean hasAlerts) {
        this.hasAlerts = hasAlerts;
    }

    public Boolean getHasAlerts() {
        return this.hasAlerts;
    }

    public UserActivity clone() throws CloneNotSupportedException {
        return (UserActivity)super.clone();
    }

}
