package client;

import java.util.Date;

public class Client {

    private static String s_id;
    private static String s_date;
    private String id;
    private String fullName;
    private String greeting;
    private Date dateOfBirth;


    public Client(String id, String fullName, String greeting) {
        super();
        this.id = id;
        this.fullName = fullName;
        this.greeting = greeting;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public static String protector(){
        return String.join("," ,s_id,s_date);
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
