package client;

/**
 * Created by djedi on 12.01.18.
 */
public interface IParsingAggregator<T> {

    default void parseLog(){}
    Object converter(T object);

}
